const cards = document.querySelectorAll('.how__card');

cards.forEach(function (card) {
  card.addEventListener('scroll', () => {
    cards.forEach(function (el) {
      el.classList.add('how__card-animation');
    })
  })
})
