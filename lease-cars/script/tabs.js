const buttons = document.querySelectorAll('.tabs__btn-item');
const cards = document.querySelectorAll('.tabs__content-card');

buttons.forEach(function (btn) {
  btn.addEventListener('click', (el) => {
    const path = el.currentTarget.dataset.path;

    buttons.forEach(function(button) {
      button.classList.remove('tabs__btn-item--active');
      el.currentTarget.classList.add('tabs__btn-item--active');
    })

    cards.forEach(function(card) {
      card.classList.remove('tabs__content-card--active');
      document.querySelector(`[data-target="${path}"]`).classList.add('tabs__content-card--active');
    })
  })
})
